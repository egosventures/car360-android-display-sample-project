package com.egosventures.car360displaysample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;

import com.egosventures.car360displaysample.R;
import com.egosventures.stable360.core.Stable360Framework;
import com.egosventures.stable360.core.activities.OscCameraViewerActivity;
import com.egosventures.stable360.core.fragments.Stable360BaseFragment;
import com.egosventures.stable360.core.fragments.ViewerFragment;
import com.egosventures.stable360.core.model.Tag;
import com.egosventures.stable360.display.fragments.OnlineViewerFragment;
import com.egosventures.stable360.display.model.OnlineViewerConfiguration;
import com.melnykov.fab.FloatingActionButton;

import java.io.File;

public class CustomOnlineViewerActivity extends FragmentActivity implements OnlineViewerFragment.OnlineViewerFragmentLoadingListener {

    // ---------------------------------------------------------------------------------------------
    // region Public attributes

    public final static String TAG = "CustomOnlineViewerAct";
    public final static String EXTRA_CONFIGURATION = "EXTRA_CONFIGURATION";

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Private attributes

    private final static String STATE_IS_INSIDE_CAPTURE_BUTTON_VISIBLE = "STATE_IS_INSIDE_CAPTURE_BUTTON_VISIBLE";
    private final static String STATE_IS_INSIDE_CAPTURE_BUTTON_ENABLED = "STATE_IS_INSIDE_CAPTURE_BUTTON_ENABLED";

    private final static String STATE_IS_PREVIOUS_TAG_BUTTON_VISIBLE = "STATE_IS_PREVIOUS_TAG_BUTTON_VISIBLE";
    private final static String STATE_IS_PREVIOUS_TAG_BUTTON_ENABLED = "STATE_IS_PREVIOUS_TAG_BUTTON_ENABLED";

    private final static String STATE_IS_SHOW_NEXT_TAG_BUTTON_VISIBLE = "STATE_IS_SHOW_NEXT_TAG_BUTTON_VISIBLE";
    private final static String STATE_IS_SHOW_NEXT_TAG_BUTTON_ENABLED = "STATE_IS_SHOW_NEXT_TAG_BUTTON_ENABLED";

    private final static int REQUEST_ACTIVITY_THETA_VIEWER = 1;

    private OnlineViewerFragment mViewer;
    private FloatingActionButton mInsideViewerButton;
    private FloatingActionButton mShowPreviousTagButton;
    private FloatingActionButton mShowNextTagButton;

    /**
     * Create a receiver to be notified in case of a framework init error
     */
    private BroadcastReceiver mFragmentMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            int event = intent.getIntExtra(OnlineViewerFragment.EXTRA_FRAGMENT_EVENT, -1);

            switch (event) {

                // Should never happens since the display SDK does not require a logged user
                case Stable360BaseFragment.EVENT_NULL_USER: {

                    Log.e(TAG, "EVENT_NULL_USER");
                    break;
                }

                case Stable360BaseFragment.EVENT_FRAMEWORK_INIT_ERROR: {

                    // Get the error:
                    Stable360Framework.Stable360FrameworkError error = (Stable360Framework.Stable360FrameworkError) intent.getSerializableExtra(Stable360BaseFragment.EXTRA_FRAMEWORK_INIT_ERROR);
                    Log.e(TAG, "EVENT_FRAMEWORK_INIT_ERROR: " + error.name());

                    break;
                }
            }
        }
    };

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Activity lifecycle

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Log.i("CustomOnlineViewerActivity", "onCreate coucou");

        setContentView(R.layout.activity_viewer);

        if (getIntent() != null && getIntent().getParcelableExtra(EXTRA_CONFIGURATION) != null) {

            OnlineViewerConfiguration configuration = getIntent().getParcelableExtra(EXTRA_CONFIGURATION);

            // Get the widgets:
            mInsideViewerButton = (FloatingActionButton) findViewById(R.id.insideViewerButton);
            mShowPreviousTagButton = (FloatingActionButton) findViewById(R.id.showPreviousTagFab);
            mShowNextTagButton = (FloatingActionButton) findViewById(R.id.showNextTagFab);

            mInsideViewerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onInsideViewerButtonClick();
                }
            });

            mShowPreviousTagButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onShowPreviousTagButtonClick();
                }
            });

            mShowNextTagButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onShowNextTagButtonClick();
                }
            });

            // Will create (or retrieve) the fragment and add it to the activity, in "R.id.onlineViewerContainer"
            mViewer = OnlineViewerFragment.getFragment(CustomOnlineViewerActivity.this, R.id.onlineViewerContainer, configuration);
            mViewer.setLoadingListener(this);

            /*
            // If you need to customize the messages displayed when framework init errors occur, you can do it this way:
            Fragment viewerFragment = new OnlineViewerFragment();
            Bundle bundle = new Bundle();

            // The spin id is a mandatory extra:
            bundle.putParcelable(OnlineViewerFragment.EXTRA_CONFIGURATION, configuration);

            // You can customize the message displayed if the framework initialisation led to a FRAMEWORK_INIT_ERROR event:
            bundle.putBoolean(OnlineViewerFragment.EXTRA_SHOW_FRAMEWORK_INIT_ERROR_MESSAGE, true);
            bundle.putString(OnlineViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR_MESSAGE, "Custom framework init error message");
            bundle.putInt(OnlineViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR_ICON, R.drawable.icon_arrow_cancel);
            bundle.putSerializable(OnlineViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR_ICON_POSITION, IconPosition.RIGHT);

            // You can customize the message displayed if the framework initialisation led to a NULL_USER event:
            bundle.putBoolean(OnlineViewerFragment.EXTRA_SHOW_NULL_USER_MESSAGE, true);
            bundle.putString(OnlineViewerFragment.EXTRA_NULL_USER_MESSAGE, "Custom null user message");
            bundle.putInt(OnlineViewerFragment.EXTRA_NULL_USER_ICON, R.drawable.icon_arrow_cancel);
            bundle.putSerializable(OnlineViewerFragment.EXTRA_NULL_USER_ICON_POSITION, IconPosition.BOTTOM);

            viewerFragment.setArguments(bundle);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.viewerFrameLayout, viewerFragment).commit();
            */
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mFragmentMessageReceiver, new IntentFilter(Stable360BaseFragment.BROADCAST_FRAGMENT_MESSAGE));
    }

    @Override
    protected void onPause() {

        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mFragmentMessageReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        
        outState.putBoolean(STATE_IS_INSIDE_CAPTURE_BUTTON_VISIBLE, mInsideViewerButton.getVisibility() == View.VISIBLE);
        outState.putBoolean(STATE_IS_INSIDE_CAPTURE_BUTTON_ENABLED, mInsideViewerButton.isEnabled());

        outState.putBoolean(STATE_IS_PREVIOUS_TAG_BUTTON_VISIBLE, mShowPreviousTagButton.getVisibility() == View.VISIBLE);
        outState.putBoolean(STATE_IS_PREVIOUS_TAG_BUTTON_ENABLED, mShowPreviousTagButton.isEnabled());

        outState.putBoolean(STATE_IS_SHOW_NEXT_TAG_BUTTON_VISIBLE, mShowNextTagButton.getVisibility() == View.VISIBLE);
        outState.putBoolean(STATE_IS_SHOW_NEXT_TAG_BUTTON_ENABLED, mShowNextTagButton.isEnabled());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);

        mInsideViewerButton.setVisibility(savedInstanceState.getBoolean(STATE_IS_INSIDE_CAPTURE_BUTTON_VISIBLE) ? View.VISIBLE : View.GONE);
        mInsideViewerButton.setEnabled(savedInstanceState.getBoolean(STATE_IS_INSIDE_CAPTURE_BUTTON_ENABLED));

        mShowPreviousTagButton.setVisibility(savedInstanceState.getBoolean(STATE_IS_PREVIOUS_TAG_BUTTON_VISIBLE) ? View.VISIBLE : View.GONE);
        mShowPreviousTagButton.setEnabled(savedInstanceState.getBoolean(STATE_IS_PREVIOUS_TAG_BUTTON_ENABLED));

        mShowNextTagButton.setVisibility(savedInstanceState.getBoolean(STATE_IS_SHOW_NEXT_TAG_BUTTON_VISIBLE) ? View.VISIBLE : View.GONE);
        mShowNextTagButton.setEnabled(savedInstanceState.getBoolean(STATE_IS_SHOW_NEXT_TAG_BUTTON_ENABLED));
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {

        // Cal super:
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REQUEST_ACTIVITY_THETA_VIEWER: {

                mInsideViewerButton.setEnabled(true);
                break;
            }
        }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region User interaction

    protected void onShowPreviousTagButtonClick() {

        mViewer.showPreviousTag(false, mViewer.isUsingTagView(), new ViewerFragment.ShowTagListener() {

            @Override
            public void onTagShown(Tag tag) {

                mViewer.setPreviousNextTagButtonsVisibilityAndEnability(mShowPreviousTagButton, mShowNextTagButton);
            }
        });
    }

    protected void onShowNextTagButtonClick() {

        mViewer.showNextTag(false, mViewer.isUsingTagView(), new ViewerFragment.ShowTagListener() {

            @Override
            public void onTagShown(Tag tag) {

                mViewer.setPreviousNextTagButtonsVisibilityAndEnability(mShowPreviousTagButton, mShowNextTagButton);
            }
        });
    }

    protected void onInsideViewerButtonClick() {

        String spinId = mViewer.getSpin().getId();
        File spinFolder = mViewer.getSpin().getFolder(CustomOnlineViewerActivity.this);
        File insideCaptureFile = new File(spinFolder, "/inside/" + spinId + ".jpg");

        if(insideCaptureFile.exists()) {

            mInsideViewerButton.setEnabled(false);

            Intent intent = new Intent(CustomOnlineViewerActivity.this, OscCameraViewerActivity.class);
            intent.putExtra(OscCameraViewerActivity.EXTRA_SPIN, mViewer.getSpin());
            intent.putExtra(OscCameraViewerActivity.EXTRA_CAN_START_TAGS_EDITION_MODE, false);
            intent.putExtra(OscCameraViewerActivity.EXTRA_CAN_RECAPTURE, false);
            intent.putExtra(OscCameraViewerActivity.EXTRA_MUST_BE_LOGGED_IN, false);
            intent.putExtra(OscCameraViewerActivity.EXTRA_REMOTE_BASE_URL, mViewer.getRemoteBaseUrl());

            startActivityForResult(intent, REQUEST_ACTIVITY_THETA_VIEWER);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region OnlineViewerFragment.OnlineViewerFragmentLoadingListener

    @Override
    public void onLoadingProgress(OnlineViewerFragment.BroadcastProgress progress) {

        // Here you may want to update your UI depending on the event. For instance, you may want to
        // display an "Open inside capture" button when the inside capture is downloaded, or hide a
        // loading progress when all the tag media have been downloaded, etc...

        Log.i(TAG, "OnlineViewerFragment loading progressed: " + progress.name());
        
        switch (progress) {

            case STARTED_INSIDE_CAPTURE_DOWNLOAD:
            case INSIDE_CAPTURE_DOWNLOADED:
            case FINISHED_FIRST_IMAGE_ANIMATION:
            case IMAGES_DOWNLOAD_PROGRESS:
            case IMAGES_DOWNLOAD_DONE:
            case FFMPEG_PROGRESS:
            case FFMPEG_DONE:
            case SPIN_FOUND_IN_CACHE_TOTALLY: {

                if(mViewer.isShowingSpin()) {

                    mViewer.setPreviousNextTagButtonsVisibilityAndEnability(mShowPreviousTagButton, mShowNextTagButton);

                    if(mViewer.hasInsideCapture() == Boolean.TRUE) {

                        mInsideViewerButton.setVisibility(View.VISIBLE);
                        mInsideViewerButton.setEnabled(mViewer.hasInsideCaptureBeenDownloaded());
                    }
                }

                break;
            }
        }
    }

    @Override
    public void onLoadingError(OnlineViewerFragment.LoadingError loadingError) {

        Log.e(TAG, "An error occured while loading the spin: " + loadingError);
    }

    // endregion
    // ---------------------------------------------------------------------------------------------
}

