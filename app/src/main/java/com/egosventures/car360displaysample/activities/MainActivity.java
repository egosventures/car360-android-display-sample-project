package com.egosventures.car360displaysample.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.egosventures.car360displaysample.R;
import com.egosventures.stable360.core.Stable360Framework;
import com.egosventures.stable360.core.Stable360FrameworksHelper;
import com.egosventures.stable360.core.activities.Stable360BaseActivity;
import com.egosventures.stable360.display.activities.OnlineViewerActivity;
import com.egosventures.stable360.display.activities.OnlineViewerConfigurationActivity;
import com.egosventures.stable360.display.fragments.OnlineViewerFragment;
import com.egosventures.stable360.display.model.OnlineViewerConfiguration;

public class MainActivity extends Activity {

    // ---------------------------------------------------------------------------------------------
    // region Attributes
    private static String TAG = "MainActivity";

    private static final int REQUEST_ONLINE_VIEWER_1 = 10;
    private static final int REQUEST_ONLINE_VIEWER_2 = 11;
    private static final int REQUEST_ONLINE_VIEWER_3 = 12;
    private static final int REQUEST_ONLINE_VIEWER_4 = 13;

    Button mTestButton1;
    Button mTestButton2;
    Button mTestButton3;
    Button mTestButton4;

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Activity lifecycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTestButton1 = (Button) findViewById(R.id.testButton1);
        mTestButton2 = (Button) findViewById(R.id.testButton2);
        mTestButton3 = (Button) findViewById(R.id.testButton3);
        mTestButton4 = (Button) findViewById(R.id.testButton4);

        Stable360FrameworksHelper.addInitListener(new Stable360Framework.InitListener() {
            @Override
            public void onInitDone() {

                mTestButton1.setEnabled(true);
                mTestButton2.setEnabled(true);
                mTestButton3.setEnabled(true);
                mTestButton4.setEnabled(true);
            }

            @Override
            public void onInitError(Stable360Framework.Stable360FrameworkError stable360FrameworkError) {}
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // All Stable360 activities can return RESULT_NULL_USER:
        if(resultCode == Stable360BaseActivity.RESULT_NULL_USER) {

            Log.e(TAG, "Login to the Stable360 framework failed");
        }

        // All Stable360 activities can return RESULT_FRAMEWORK_INIT_ERROR:
        else if(resultCode == Stable360BaseActivity.RESULT_FRAMEWORK_INIT_ERROR) {

            Stable360Framework.Stable360FrameworkError error = (Stable360Framework.Stable360FrameworkError) data.getSerializableExtra(Stable360BaseActivity.EXTRA_FRAMEWORK_INIT_ERROR);
            Log.e(TAG, "Stable360 framework initialisation failed: " + error.name());
        }

        // Handle specific activities results:
        else {

            switch (requestCode) {

                case REQUEST_ONLINE_VIEWER_1:
                case REQUEST_ONLINE_VIEWER_2:
                case REQUEST_ONLINE_VIEWER_3:
                case REQUEST_ONLINE_VIEWER_4: {

                    switch (resultCode) {

                        case OnlineViewerActivity.RESULT_LOADING_ERROR: {

                            OnlineViewerFragment.LoadingError error = (OnlineViewerFragment.LoadingError) data.getSerializableExtra(OnlineViewerActivity.EXTRA_LOADING_ERROR);

                            Log.e(TAG, "An error occurred while loading the remote spin: " + error.name());

                            break;
                        }
                    }
                }
            }
        }
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Utils

    /**
     * This sample uses the OnlineViewerFragment
     *
     * OnlineViewerConfiguration:
     *
     * Create a OnlineViewerConfiguration to configure the viewer and pass it to the Intent.
     *      Required configuration:
     *          - carId: the carId to load (VIN number, licence plate, custom code or spinCode)
     *      Example of optional configurations:
     *          - resolution: spin resolution (see OnlineViewerConfiguration.SupportedResolution)
     *          - customRootUrl: if set, spin images/videos will be searched in `customRootUrl/carId`
     *          - showFirstImageBeforeStartingLoading
     *          - ...
     *          (see documentation for more information about the configurations)
     */
    public void onTest1ButtonClicked(View v) {

        // 3afb9fdbeadafd2962953bd3b2f668c6 // Has no video
        // 3e7802c1cd69f08f2a3bae389816ece6 // Has no video

        // 0569e9354c5bcabae747ed3ec659df95 // Dark grey Honda
        // 00b0febccc32ff4dd856c1d9dcc09249 // White BMW
        // 0f000dcb03f8c951873b8bbb06843a17 // Blue Hyundai
        // ffeec596685d98006b48234e0eef7e17 // Red Subaru
        // 9a9d581a77d402074fc42c298820da2a // Black Chevrolet

        OnlineViewerConfiguration configuration = new OnlineViewerConfiguration();
        configuration.carId = "CAR360_BLUE_BMW_M4";

        Intent intent = new Intent(MainActivity.this, CustomOnlineViewerActivity.class);
        intent.putExtra(CustomOnlineViewerActivity.EXTRA_CONFIGURATION, configuration);
        startActivityForResult(intent, REQUEST_ONLINE_VIEWER_1);
    }

    /**
     * This sample uses the OnlineViewerActivity
     */
    public void onTest2ButtonClicked(View v) {

        OnlineViewerConfiguration configuration = new OnlineViewerConfiguration();
        //configuration.carId = "3afb9fdbeadafd2962953bd3b2f668c6";
        configuration.carId = "c73b2b43277953f7385c52513c91f6d0";
        configuration.firstImageResolution = OnlineViewerConfiguration.SupportedResolution.RESOLUTION_240_135;
        configuration.resolution = OnlineViewerConfiguration.SupportedResolution.RESOLUTION_640_360;

        Intent intent = new Intent(this, OnlineViewerActivity.class);
        intent.putExtra(OnlineViewerActivity.EXTRA_CONFIGURATION, configuration);
        startActivityForResult(intent, REQUEST_ONLINE_VIEWER_2);
    }

    /**
     * Sample of OnlineViewer with a custom root URL.
     */
    public void onTest3ButtonClicked(View v) {

        OnlineViewerConfiguration configuration = new OnlineViewerConfiguration();

        configuration.customRootUrl = "http://s3-us-west-2.amazonaws.com/car360spinsresized/bfb32f72f6a9f9dabd63d2df6a9d3823/ffeec596685d98006b48234e0eef7e17";

        configuration.resolution = OnlineViewerConfiguration.SupportedResolution.RESOLUTION_1920_1080;
        configuration.firstImageResolution = OnlineViewerConfiguration.SupportedResolution.RESOLUTION_1920_1080;

        Intent intent = new Intent(this, OnlineViewerActivity.class);
        intent.putExtra(OnlineViewerActivity.EXTRA_CONFIGURATION, configuration);
        startActivityForResult(intent, REQUEST_ONLINE_VIEWER_3);
    }

    /**
     * OnlineViewerConfigurationActivity, .
     */
    public void onTest4ButtonClicked(View v) {

        Intent intent = new Intent(this, OnlineViewerConfigurationActivity.class);
        startActivityForResult(intent, REQUEST_ONLINE_VIEWER_4);
    }

    // endregion
    // ---------------------------------------------------------------------------------------------
}
